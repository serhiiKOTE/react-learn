import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import App from './components/app';
import TodoService from './servises/todo-test-service';
import {TodoServiceProvider} from './components/todo-service-context';

import store from './store';

const todoService = new TodoService();

ReactDOM.render(
  <Provider store={store}>
    <TodoServiceProvider value={todoService}>
      <App/>
    </TodoServiceProvider>
  </Provider>
  , document.getElementById('root'));
