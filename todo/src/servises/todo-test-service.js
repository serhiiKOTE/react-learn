export default class TodoService {

  data = [
    {
      id: 1,
      label: 'Test Service Todo 1',
      important: false,
      done: false
    },
    {
      id: 2,
      label: 'Test Service Todo 2',
      important: false,
      done: false
    },
    {
      id: 3,
      label: 'Test Service Done Todo 3',
      important: false,
      done: true
    },
    {
      id: 4,
      label: 'Test Service Important Todo 4',
      important: true,
      done: false
    },
    {
      id: 5,
      label: 'Test Service Important Todo 5',
      important: true,
      ddone: false
    },
    {
      id: 6,
      label: 'Test Service Important Todo 6',
      important: true,
      done: false
    },
    {
      id: 7,
      label: 'Test Service Important Done Todo 7',
      important: true,
      done: true
    }
  ];

  /**
   * Return the data wish a delay, and generating a error randomly
   * @returns {Promise<any>}
   */
  getTodos() {
    return new Promise((resolve, reject) => {

      setTimeout(() => {
        if (Math.random() > 0.75) {
          reject(new Error('Something bad happened'))
        } else{
          resolve(this.data)
        }
      }, 700)
    });
  }
}
