import {
  FETCH_TODO_FAILURE,
  FETCH_TODO_REQUEST,
  FETCH_TODO_SUCCESS,
  TODO_ADDED_TO_LIST,
  TODO_DELETED_FROM_LIST, TODO_TOGGLED_DONE, TODO_TOGGLED_IMPORTANT
} from "../constants/actions";
import {createTodo, getQuantityDone, toggleProperty, updateTodos} from "../utils/helpers";


const initialState = {
  loading: true,
  error: null,
  todos: [],
  quantity: 0,
  quantityDone: 0
};

export default function todos(state = initialState, action) {
  switch (action.type) {
    case FETCH_TODO_REQUEST:
      return {
        todos: [],
        loading: true,
        error: null,
        quantity: 0,
        quantityDone: 0
      };

    case FETCH_TODO_SUCCESS: {
      const todos = action.payload;

      return {
        todos: todos,
        loading: false,
        error: null,
        quantity: todos.length,
        quantityDone: getQuantityDone(todos)
      };
    }

    case FETCH_TODO_FAILURE:
      return {
        todos: [],
        loading: false,
        error: action.payload,
        quantity: 0,
        quantityDone: 0
      };

    case TODO_ADDED_TO_LIST: {
      const newTodo = createTodo(state, action.payload);
      const newTodos = updateTodos(state.todos, newTodo);

      return {
        ...state,
        todos: newTodos,
        loading: false,
        error: false,
        quantity: newTodos.length
      };
    }

    case TODO_DELETED_FROM_LIST: {
      const newTodos = state.todos.filter(({id}) => id !== action.payload);
      return {
        ...state,
        todos: newTodos,
        quantity: newTodos.length,
        quantityDone: getQuantityDone(newTodos)
      };
    }

    case TODO_TOGGLED_IMPORTANT: {
      const newTodos = toggleProperty(state.todos, action.payload, 'important');

      return {
        ...state,
        todos: newTodos
      };
    }

    case TODO_TOGGLED_DONE: {
      const newTodos = toggleProperty(state.todos, action.payload, 'done');

      return {
        ...state,
        todos: newTodos,
        quantityDone: getQuantityDone(newTodos)
      };
    }

    default:
      return state;
  }
};
