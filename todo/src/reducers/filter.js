import {
  TODO_TOGGLED_FILTER,
  TODO_STRING_FILTER
} from '../constants/actions';

const initialState = {
  filter: 'SHOW_ALL',
  filterString: ''
};

export default function filter(state = initialState, action) {
  switch (action.type) {

    case TODO_TOGGLED_FILTER: {
      return {
        ...state,
        filter: action.payload
      };
    }

    case TODO_STRING_FILTER: {
      return {
        ...state,
        filterString: action.payload
      };
    }

    default:
      return state;
  }
};
