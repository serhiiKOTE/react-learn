import {
  FETCH_TODO_REQUEST,
  FETCH_TODO_SUCCESS,
  FETCH_TODO_FAILURE,
  TODO_ADDED_TO_LIST,
  TODO_DELETED_FROM_LIST,
  TODO_TOGGLED_IMPORTANT,
  TODO_TOGGLED_DONE,
  TODO_TOGGLED_FILTER,
  TODO_STRING_FILTER
} from '../constants/actions';

const todosRequested = () => {
  return {
    type: FETCH_TODO_REQUEST
  };
};

const todosLoaded = (newTodos) => {
  return {
    type: FETCH_TODO_SUCCESS,
    payload: newTodos
  };
};

const todosError = (error) => {
  return {
    type: FETCH_TODO_FAILURE,
    payload: error
  };
};

export const todoAddedToList = (label) => {
  return {
    type: TODO_ADDED_TO_LIST,
    payload: label
  };
};

export const todoDeletedFromList = (todoId) => {
  return {
    type: TODO_DELETED_FROM_LIST,
    payload: todoId
  };
};

export const todoToggleImportant = (todoId) => {
  return {
    type: TODO_TOGGLED_IMPORTANT,
    payload: todoId
  };
};

export const todoMarkedDone = (todoId) => {
  return {
    type: TODO_TOGGLED_DONE,
    payload: todoId
  };
};

export const todoFiltered = (filter) => {
  return {
    type: TODO_TOGGLED_FILTER,
    payload: filter
  };
};

export const todoStringFiltered = (str) => {
  return {
    type: TODO_STRING_FILTER,
    payload: str
  };
};

export const fetchTodos = (todoService, dispatch) => () => {
  dispatch(todosRequested());
  todoService.getTodos()
    .then((data) => {
      dispatch(todosLoaded(data))
    })
    .catch((err) => dispatch(todosError(err)));
};
