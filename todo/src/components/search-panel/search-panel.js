import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {todoStringFiltered} from "../../actions";
import './search-panel.css';

const SearchPanel = ({filterString, onSearchChange}) => {
  return (
    <input
      placeholder="Filter todos"
      className="form-control search-input"
      value={filterString}
      onChange={(e) => onSearchChange(e.target.value)}/>
  )
};

const mapStateToProps = ({filter: {filterString}}) => {
  return {filterString};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    onSearchChange: todoStringFiltered
  }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPanel);
