import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {todoFiltered} from "../../actions";
import * as filterTypes from '../../constants/filters-type';

import './item-status-filter.css';

const ItemStatusFilter = (props) => {

  const buttons = [
    {
      filterType: filterTypes.SHOW_ALL,
      label: 'All'
    },
    {
      filterType: filterTypes.SHOW_ACTIVE,
      label: 'Active'
    },
    {
      filterType: filterTypes.SHOW_DONE,
      label: 'Done'
    }
  ];

  const {filter, onFilterChange} = props;

  const buttonsArr = buttons.map(({filterType, label}) => {
    const isActive = filter === filterType;
    const classNames = isActive ? 'btn-info' : 'btn-outline-secondary';
    return (
      <button key={filterType}
              type="button"
              className={`btn  ${classNames}`}
              onClick={() => onFilterChange(filterType)}
              title={`Toggle ${label}`}>
        {label}
      </button>
    );
  });

  return (
    <div className="btn-group">
      {buttonsArr}
    </div>
  );

};

const mapStateToProps = ({filter: {filter}}) => {
  return {filter}
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    onFilterChange: todoFiltered
  }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemStatusFilter);
