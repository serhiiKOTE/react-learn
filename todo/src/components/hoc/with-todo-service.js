import React from 'react';
import {TodoServiceConsumer} from '../todo-service-context'

const WithTodoService = () => (Wrapped) => {

  return (props) => {
    return (
      <TodoServiceConsumer>
        {
          (todoService) => {
            return (<Wrapped {...props} todoService={todoService}/>); // Добавляє в пропс todoService
          }
        }
      </TodoServiceConsumer>
    );
  };
};

export default WithTodoService;
