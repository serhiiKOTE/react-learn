import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {
  todoDeletedFromList,
  todoToggleImportant,
  todoMarkedDone
} from "../../actions";
import './todo-list-item.css';

class TodoListItem extends Component {

  constructor(props) {
    super(props);
    this.onDeleted = this.onDeleted.bind(this);
    this.onToggleImportant = this.onToggleImportant.bind(this);
    this.onToggleDone = this.onToggleDone.bind(this);
  }

  onDeleted() {
    this.props.onDeleted(this.props.id);
  }

  onToggleImportant() {
    this.props.onToggleImportant(this.props.id);
  }

  onToggleDone() {
    this.props.onToggleDone(this.props.id);
  }

  render() {
    const {label, done} = this.props;

    let classNames = "todo-list-item";

    if (done) {
      classNames += ' done';
    }

    return (
      <span className={classNames}>
      <span
        className="todo-list-item-label"
        onClick={this.onToggleDone}>
        {label}
      </span>

      <button type="button"
              className="btn btn-outline-success btn-sm float-right"
              onClick={this.onToggleImportant}>
        <i className="fas fa-exclamation"/>
      </button>
      <button type="button"
              className="btn btn-outline-danger btn-sm float-right"
              onClick={this.onDeleted}>
        <i className="fas fa-trash-alt"/>
      </button>
    </span>
    );
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    onDeleted: todoDeletedFromList,
    onToggleImportant: todoToggleImportant,
    onToggleDone: todoMarkedDone
  }, dispatch)
};

export default connect(null, mapDispatchToProps)(TodoListItem);
