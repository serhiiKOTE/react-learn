import React from 'react';
import {connect} from 'react-redux';
import {todoAddedToList} from '../../actions';

import './item-add-form.css';

const ItemAddForm = ({todoAddedToList}) => {
  let input;

  return (
    <form className="item-add-form mt-3" onSubmit={
      (e) => {
        e.preventDefault();

        if (!input.value.trim()) {
          return false;
        }

        todoAddedToList(input.value);
        input.value = ''
      }
    }>
      <div className="form-group">
        <div className="input-group">
          <input ref={node => input = node}
                 className="form-control"
                 placeholder="What needs to be done"/>
          <div className="input-group-append">
            <button type="submit" className="btn btn-primary" title="Add a new Todo">Add Todo</button>
          </div>
        </div>
      </div>
    </form>
  );
};

const mapStateToProps = ({todos: {todos}}) => {
  return {todos}
};

const mapDispatchToProps = {todoAddedToList};

export default connect(mapStateToProps, mapDispatchToProps)(ItemAddForm);
