import React, {Component} from "react";
import {connect} from "react-redux";
import {fetchTodos} from "../../actions";
import * as filterTypes from "../../constants/filters-type";
import {compose} from "../../utils";
import WithTodoService from "../hoc";
import TodoList from "./todo-list";

class TodoListContainer extends Component {

  componentDidMount() {
    this.props.fetchTodos();
  }

  render() {
    const {todos, loading, error} = this.props;

    if (loading) {
      return <p>Loading...</p>;
    }

    if (error) {
      return <p>Error...</p>;
    }

    return <TodoList todos={todos}/>;
  };
}

const getFilteredTodos = (todos = [], filter) => {
  switch (filter) {
    case filterTypes.SHOW_ALL:
      return todos;
    case filterTypes.SHOW_DONE:
      return todos.filter(todo => todo.done);
    case filterTypes.SHOW_ACTIVE:
      return todos.filter(todo => !todo.done);
    default:
      throw new Error('Unknown filter: ' + filter);
  }
};

const getStringFilteredTodos = (todos = [], filterString) => {

  if (filterString.trim().length === 0) {
    return todos;
  }

  return todos.filter((todo) => {
    return todo.label.toLowerCase().indexOf(filterString.toLowerCase()) > -1;
  });

};

const mapStateToProps = ({todos: {todos, loading, error}, filter: {filter, filterString}}) => {
  return {
    todos: getStringFilteredTodos(getFilteredTodos(todos, filter), filterString),
    loading,
    error,
    filterString
  };
};

const mapDispatchToProps = (dispatch, {todoService}) => {
  return {
    fetchTodos: fetchTodos(todoService, dispatch)
  };
};

export default compose(
  WithTodoService(),
  connect(mapStateToProps, mapDispatchToProps)
)(TodoListContainer);