import React from 'react';
import TodoListItem from '../todo-list-item';

import './todo-list.css';

const TodoList = ({todos}) => {

  const elements = todos.map((item) => {
    const {id, important, ...itemProps} = item;

    let classNames = "list-group-item todo-list-item";

    if (item.done) {
      classNames += ' todo-list-item--done';
    }

    if (important) {
      classNames += ' todo-list-item--important';
    }

    return (
      <li key={id} className={classNames}>
        {id}
        <TodoListItem
          {...itemProps}
          id={id}
        />
      </li>
    );
  });

  return (
    <ul className="todo-list list-group">
      {elements}
    </ul>
  );
};

export default TodoList;
