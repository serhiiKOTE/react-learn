import React from 'react';
import {connect} from "react-redux";

const QuantityPanel = ({quantity,quantityDone}) => {
  return (
    <span className="h2">{quantity - quantityDone} more to do, {quantityDone} done</span>
  );
};

const mapStateToProps = ({todos: {quantity, quantityDone}}) => {
  return {quantity, quantityDone};
};

export default connect(mapStateToProps)(QuantityPanel);
