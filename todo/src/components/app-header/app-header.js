import React from 'react';
import QuantityPanel from './quantity-panel'
import './app-header.css';

const AppHeader = () => {
  return (
    <div className="app-header d-flex justify-content-lg-between align-items-lg-baseline">
      <h1>List of important doings</h1>
      <QuantityPanel/>
    </div>
  );
};

export default AppHeader;
