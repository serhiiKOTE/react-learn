import React, {Component} from 'react';
import {connect} from 'react-redux';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import ItemAddForm from '../item-add-form';

import './app.css';

class App extends Component {

  render() {
    return (
      <div>
        <AppHeader/>
        <div className="top-panel d-flex mb-3">
          <SearchPanel/>
          <ItemStatusFilter/>
        </div>
        <TodoList/>

        <ItemAddForm/>
      </div>
    );
  }
}

const mapStateToProps = ({todos: {todos, loading, error}}) => {
  return {
    todos: todos,
    loading: loading,
    error: error
  }
};

export default connect(mapStateToProps)(App);
