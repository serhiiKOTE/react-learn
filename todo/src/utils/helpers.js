export const updateTodos = (todos, item) => {
  return [
    ...todos,
    item
  ];
};

export const createTodo = ({todos}, label) => {
  const id = todos.reduce(function (max, {id}) {
    return max < id ? id : max;
  }, 0);

  return {
    id: id + 1,
    label: label,
    important: false,
    done: false
  };
};

export const toggleProperty = (arr, id, propName) => {
  return arr.map((todo) => {
    if (todo.id === id) {
      todo[propName] = !todo[propName];
    }
    return todo;
  });
};

export const getQuantityDone = (arr) => {
  return arr.reduce((qty, item) => {
    return item.done ? ++qty : qty;
  }, 0);
};