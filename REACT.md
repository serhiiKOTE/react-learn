## Rest parameter 

function (a, b, ...numbers) {
}
* останній у ф-ціі
* більше одного параметра

## Spread operator

const arr = [0, 2, 4];
const arr2 = [5, 3, 7];

const res = Math.max(41, ...arr, ...arr2, 10);

const sallowCopy = [...arr, ...arr2, 21]

## Destructuring

Достати значення з структури даних, масива чи обєкту
Зберігає в змінні значення полів в структурі

const person = {
    fn: 'first name',
    ln:'last name'
}
let fn = person.fn; // old
let ln = person.ln; // old

const {fn, ln} = person;
log(fn, ln);  // => first name last name

const person = {
    name: {
        fn: 'first name',
        ln:'last name'
    }
}

const {name: {fn, ln}} = person;
Достає тільки fn, ln
log(fn, ln); // => first name last name

* Перейменування  
const {name: {fn: first, ln: last}} = person;
Достає тільки first, last
log(first, last); // => first name last name
* Значення по замовчуванню (вложено буде помилка)  
const {role = 'user'} = person;
log(role); // => user

const { permission : {role = 'user'} = {} } = person;

### Деструктуризація аргументів ф-ції

function connect ({ 
    host = 'localhost',
    port = 12313,
    user = 'guest'} = {}) {  // = {} обєкт по замовчуванню, якщо не буде то при connect(); буде помилка

}

## Деструктуризація масивів

const fib = [1, 1, 2, 3, 5, 8, 13];

const [, a, , b] = fib;
log(a,b); // => user

const fib = [[1, 1], [2, 3]];

const [[x1, y1], [x2, y2]] = fib;
log(x1, y1, x2, y2);

## Шаблонні строки
const a = 5;
const text = `Hello ${a}`;
log(text); // Hello 5

## Обєкти
x = 10;
y = 20;

point = {
    x, 
    y,
    draw(var){
    }
};

defaults = {
    a: 1,
    b: 2
}
user = {
    b: 4,
    c: 5
}

### shallowCopy

var res = Object.assign({}, defaults, user)
log(res);
res = {
    a: 1,
    b: 4,
    c: 5
}

## Spread operator for Objects 2018

defaults = {
    a: 1,
    b: 2
}
user = {
    b: 4,
    c: 5
}

const res = { ...defaults, ...user}

## Prototypes

### Цепочка прототипів
Якщо властивість є в обєкті, то берем його,
    якщо ні, то перевіряємо на наявність прототипу,
        якщо в прототипі є властивість то берем його,
            якщо ні, то йдемо по всій глибині аж до обєкта в якого немає прототипу Object.prototype
                якщо нема і там то undefined

Добавити прототип обєкту:
* Object.setPrototypeOf(obj, proto) // затратно по ресурсах
* Object.create(proto)
* using obj = new Animal() // constructor

Без прототипа: * Object.create(null)

## Classes

```
class Animal {
    constructor (name, voice) {
        this.name = name;
        this.voice = voice;
    } 
    say() {
        log(this.name + this.voice);
    }
}

// duck -> Bird.prototype -> Animal.prototype -> Object.prototype -> null
class Bird extends Animal {
    constructor (name, voice, canFly) {
        super(name, voice); // до того як присвоюємо this
        this.canFly = canFly;
    } 
}

const duck = new Bird ('Duck', 'quack', true);
duck.say();
```

## Iterator
Допомагає обходити структури даних
```
range[Symbol.iterator] = function() {

  let current = this.from;
  let last = this.to;

  // метод должен вернуть объект с методом next()
  return {
    next() {
      if (current <= last) {
        return {
          done: false,
          value: current++
        };
      } else {
        return {
          done: true
        };
      }
    }

  }
};
```


## Generator 
Ф-ція яка може сере ставити на паузу

```function* name ()```


## Class properties

1. можна ініціювати властивості прямо в тілі класу 
2. ф-ції з привязаним this (bind) 
3. static - Ф-ція чи Властивість належить Класу а не Обєкту (логічно групувати ф-ції)
```
class Counter {
    count = 1; // 1
    
    inc = () => {
        this.count += this.incStep;
    }
    
    static incStep = 2;
    
    static icrementAll = function (arr) {
        arr.forEach((c) => c.inc());
    };
}
setTimeout(cnt.inc, 1000); // 2

Counter.icrementAll([]);
```

## Modules

* перейменовування: add adefaults a
* тільки один default  
Локальні:
```
ecport {
    add, sub, multiplay // іменовані експорти
};
ecport default Graph; // default

import { add as a, multiplay } from './path'; // іменовані експорти
import * as calc from './path'; // все в обєкт
import Graph from './path'; // default
import Graph, * as calc from './path'; // Міксування
import './path'; // Для побічних ефектів без експортування
```

* глобальні залежності (пакети npm ...)
import lib from 'name-lib'; // lib

# REACT

* Компоненти присами з великої букви
* Повертати потрібно тільки один реакт компонент (обернути в div)
* class = className


## Колекції і ключі
Добаволяти унікальні ключі для компонентів, по замовчуванню 1,2,3
Для перерисовки.
Для добавлення ноди в початок, реакт буде порівнювати по ключу
Для ключа найкраще використовувати id, будуть унікальними. Не використовувати случайні одиниці чи індекси масива, так як вони можуть змінитись

## 32 Як імпортувати css

## Компоненти

1) Stateless Functional Components (без состояния) Функціональні
2) Stateful Components (з состоянием) просто компоненти, Компоненти-Класи

## Свойства Компонентів (PROPS)

- PROPS Компонентів незмінні
- STATE компонента належить самому компоненту
- значення свойств STATE компонента не змінюються а замінюються на нові (setState)

## Композиція
це складання маленких частин в едине, для створення більших частин (розділення логіки додатка на ф-ції, кожен з яких виконує частину роботи.
+ спрощує код, дозволяє його використовувати в інших частинах додатка

## 34 Компоненти класи
Якщо потрібно працювати з станом то робити клас

Компонент це:
* самостійна частина приложения
* містить частину логіки
* описує свій вид відносно state
* можна використовувати багато разів в різних місцях

## 37 Як працює setState
на пряму свойства мыняти не можна, тыльки через setState(), вона сама все правильно зробе

## 38 Зміна стану, залежно від попереднього стану
setState() - асинхроння ф-ція
потрібно передавати ф-цію
```
this.onMarkImportant = () => {
  this.setState((state) => {
    return {
      important: !state.important
    }
  });
}
```
Ф-ція визивається асинхронно, і не факт, що стан вже змінився.
Виникає багато помилок які складно зловити, так як проявляються через раз

## 39 Власні собития

## 40 Видалення елементів
не змінювати стейт, 
копіювати і повертати новий стейт

## 41 Додавання елементів
не змінювати стейт, 
копіювати і повертати новий стейт

## 42 Дані в Реакт додатку
* централізоване зберігання даних
* якщо дані потрбіно використовувати в декількох компонентах - їх потрібно зберігати в батьківському компоненті
* щоб передати дані на верх по ієрархії компонентів, використовувати собития

## 43 setState редагування елементів

## 44 Робота з формами

## 45 Контрольовані елементи Controlled
щоб зробити елемент контрольованим, потрібно зробити так щоб value компонента встановлювалось з state компонента

# Принципи

* Реакт не знає про роботу сервера
* Мережевий код повинен бути ізольований від компонентів
* Трансформувати дані, якщо необхідно, до того як їх отримає компонент
* Опрацьовувати стани "Завантаження", "Помилка"
* Розділяти відповідальність компонентів: логіку і рендеринг

# Life cycle 

1) Ініціалізація компонента (перший рендер) MOUNT  

    - getDefaultProps 
    - getInitialState
    - componentWillMount
    - render
    - componentDidMount

2) Зміна параметрів (props)  

    - componentWillReceiveProps (nextProps)
    - shouldComponentUpdate (nextProps, nextState)
    - componentWillUpdate (nextProps, nextState)
    - render
    - componentDidUpdate (prevProps, prevState)

3) Зміна параметрів (props)  

    - shouldComponentUpdate (nextProps, nextState)
    - componentWillUpdate (nextProps, nextState)
    - render
    - componentDidUpdate (prevProps, prevState)

4) Демонтаж Unmount 

    - componentWillUnmount (nextProps, nextState) уборка після компонента, навішані собития, очищають ссилки які зберігались в компоненті, clearInterval()


### MOUNTING
Перша ініціалізація компонента, не враховуються як обновление, тому componentDidUpdate визиватись не буде
constructor () => render () => componentDidMount()

dom елементи проініціалізовані.

Коли потрібно апі чи сетТайм аут то використовувати componentDidMount
```
    if (this.props.personId !== prevProps.personId) {
      this.updatePerson();
    }
```

потрібно робити перевірку щоб не ввести в безкінечний цикд.
Обновляємо коли ІД різні
### UPDATES
Цей метод викликається коли компонент обновився, за рахунок ногого props чи отримав новий стейт setState()
new props
            => render () => componentDidUpdate(prevProps, prevState)
setState() 

### UNMOUNTING
componentWillUnmount()

Викликаэться перед тим як компонент маэ очиститись, поки що є в ДОМ на сторінці
Цей метод для того щоб очистити всі ресурси з якими працював компонент, якщо потрібно
* відміняти запити до сервера
* відписуватись від вебсокетів
* відміняти setInterval

### ERROR
componentDidCatch()

Обробка не пійманих помилок в інших методах lifecicle React компонента

* тільки в методах lifecicle React компонента (render, ...методи які впливають на стан і рендеринг)
* не будуть оброблятись в обробниках onClick, чи асинхронних коллБеках, запити до сервера, навіть якщо вони записані в методі lifecicle

## Передача ф-цій в props
Можна передавати не тільки на онКлік, а й ф-цію для отримання даних чи рендерингу

## Render ф-ція
патерн у якому в компонент передається ф-ція для рендерингу частини або всього компонента

Ф-ція, яка генерує рельний DOM з віртуального

За віртуальним ДОМ простіше слідкувати, і його можна швидко створити/змінити
Реакт відповідає за те щоб реальний ДОМ відповідав віртуальному ДОМ
Реакт використовує спеціальний алгоритм пошуку різниці у віртуальному ДОМ до і після змін

## Свойства-елементи

## Children
Погана назва свойства
```<Component>Children</Component>

this.props.children
```

## props.children

## Клонування елементів

## Компоненти вищого порядку (HOC) патерн

ф-ція приймає компонент і повертає компонент
Використовується для створення компонентів-контейнерів,
для отримання даних від серверу чи redux,
потім передає в вигляді props в дочірній компонент

## Default Props
* для ф-цій
```
ComponentName.defaultProps = {
  onItemSelected: () => {}
};
```
* для класів
```
  static defaultProps = {
    updateInterval: 30000
  };
```

## propTypes
```
  static propTypes = {
    updateInterval: (props, propName, componentName) => {
      const value = props[propName];
      if (typeof value === 'number' && !isNaN(value)) {
        return null;
      }
      return new TypeError(`${componentName}: ${propName} must be number`);
    }
  };
```
```
ItemList.propTypes = {
  onItemSelected: PropTypes.func,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  children: PropTypes.func.isRequired
};

Row.propTypes = {
  left: PropTypes.node,
  right: PropTypes.node
};

```

# Роутинг

## Router, Route, Link
```
<Router>
    <div className="stardb-app">
      <Header onServiceChange={this.onServiceChange}/>
      <RandomPlanet updateInterval={10000}/>
    
      <Route path="/"
             render={() => <h2>Welcome to StarDb</h2>}
             exact={true}/>
      <Route path="/people" component={PeoplePage}/>
      <Route path="/planets" component={PlanetPage}/>
      <Route path="/starships" component={StarshipPage}/>
    
    </div>
</Router>
```

```
<Link to="/people">People</Link>
```
* в Роут можна передавати Рендер ф-цію,
* працює як фільтр, зрівнює path
* Якщо створити кілька роутів з однаковим path, то виведуться всі в тій послідовності, в якій обявлені
* перевіряє path на наявнысть в ньому location.pathname з адресної стрічки
Навіть якщо в location.pathname буде ще щось, всеодно спрацює
```
<Route path="/" render={() => <h2>Welcome to StarDb</h2>}/>
<Route path="/people" component={PeoplePage}/>
```
виведуться обидва на people
* Exact параметр для точного співпадання path
```
<Route path="/" render={() => <h2>Welcome to StarDb</h2>} exact={true}/>
<Route path="/people" component={PeoplePage}/>
```
виведуться по одному на path

## Динамічні шляхи
* В Роут можна передати параметри
```
<Route   path="/starships/:id"
         render={({match}) => {
           const {id} = match.params;
           return <StarshipDetails itemId={id}/>
         }}/>
```
* `:id` може бути будьякою стрічкою, яка йде після  `/starships/`

## withRouter
* це НОС який передає компоненту обєкти рекакт роуьер
```
const MyComponent = ({ match, location, history }) => {
    return (
        <Button
            onClick={() => history.push(`/new/path`)}
        > Click </Button>
    );
};

export default withRouter(MyComponent);
```
## Відносні шляхи
* шляхи
```
history.push('/person') // абсолютний шлях
history.push('person') // відносний шлях
```
* Закриваючий слеш важливий

```
history.push('person') 
```
поточний шлях /site/catalog/
результат     /site/catalog/person  

поточний шлях /site/catalog (без слеша)
результат     /site/person

## Опціональні параментри
Адреса містить id відкритого елемента, при перезагрузці він відкриваеться
```
<Route path="/people/:id?" component={PeoplePage}/>
```

## Redirect

## Switch обробка неіснуючих URL
```
<Switch>
    <Route path="/"
           render={() => <h2>Welcome to StarDb</h2>}
           exact={true}/>
    <Route path="/people/:id?" component={PeoplePage}/>
    <Route path="/planets" component={PlanetPage}/>
    <Route path="/starships" exact component={StarshipPage}/>
    <Route path="/starships/:id"
           render={({match}) => {
             const {id} = match.params;
             return <StarshipDetails itemId={id}/>
           }}/>
    <Route path="/login"
           render={() => (
             <LoginPage isLoggedIn={isLoggedIn} onLogin={this.onLogin}/>
           )}/>
    <Route path="/secret"
           render={() => (
             <SecretPage isLoggedIn={isLoggedIn}/>
           )}/>
    
    
    <Route render={()=> <h2>Page not found</h2>}/> АБО <Redirect to="/"/>
</Switch>
```

# REDUX

View (генерує) -> Action -> Dispatcher -> Store -> View  і так по кругу

* вирішує проблему управління состоянием в додатку
* пропонує зберігати state в одному глобальному обєкті
* ф-ція Reducer обновляє глобальний стейт в відповідь на Actions (Обєкти)
* обєкт Store координує обновлення

![alt text](image/redux.jpg)

Правила:
* отримує поточний state і Action
* Action простий обєкт в якого є поле action.type
* В залежності від action.type втконуємо якусь дію
* якщо action.type немає в списку - повертаємо state в тому вигляді, в якому його отримали (Без Змін) 
* якщо state === undefined - повернути значення по замовчуванню initial state

```
const reducer = (state = 0, action) => {

  switch (action.type) {
    case 'INC':
      return state + 1;

    default:
      return state;
  }

};

let state = reducer(undefined, {});
console.log(state);

state = reducer(state, {type: 'INC'});
console.log(state);

state = reducer(state, {type: 'INC'});
console.log(state);
```

## Redux Store

```
const store = createStore(reducer);

store.subscribe(()=>{
  console.log(store.getState());
});

store.dispatch({type: 'INC'});
store.dispatch({type: 'INC'});
```

## Чисті ф-цїї

* значення яке повертається залежить тільки від аргументів
* немає побічниї ефектів (може змінювати тільки свої локальні змінні, не можна змінювати навіть аргументи, додавати елементи в Дом, Таймаути, случайні числа, запити до сервера)

## Action Creator

ф-ція яка створює обєкти action.  
Спрощує код

```
const rnd = (payload) => ({type: 'RND', payload});
store.dispatch({type: 'RND'}, payload);

const rnd = (payload) => ({type: 'RND', payload});
const payload = Math.floor(Math.random()*10);
store.dispatch(rnd(payload));
```

## bindCationCreator

## mapDispatchToProps

## Enhancer - контролює процес створення Store

## Middleware - модифікує то як працює ф-ція Dispatch




